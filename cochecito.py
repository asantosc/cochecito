import unittest

class Coche:
    def __init__(self, color,marca, modelo,matricula, velocidad=0):
        self.color=color
        self.marca = marca
        self.modelo = modelo
        self.matricula=matricula
        self.velocidad = velocidad

    def acelerar(self, cantidad):
        self.velocidad += cantidad
        return self.velocidad

    def frenar(self, cantidad):
        self.velocidad -= cantidad
        if self.velocidad < 0:
            self.velocidad = 0
        return self.velocidad
    def mostrar_informacion(self):
        return f"Marca: {self.marca}, Modelo: {self.modelo}, Matrícula: {self.matricula}, Velocidad actual: {self.velocidad} km/h"

    def calcular_t(self, distancia):
        if self.velocidad == 0:
            return "Infinito"
        else:
            tiempo_horas = distancia / self.velocidad
            return f"Tiempo estimado de llegada: {tiempo_horas:.2f} horas"

class TestCoche(unittest.TestCase):

    def setUp(self):
        self.mi_coche = Coche('Rojo','Ferrari', 'Testarrosa','1234BCD')

    def test_acelerar(self):
        self.mi_coche.acelerar(70)
        self.assertEqual(self.mi_coche.velocidad, 70)

    def test_frenar(self):
        self.mi_coche.acelerar(70)
        self.mi_coche.frenar(50)
        self.assertEqual(self.mi_coche.velocidad, 20)
        self.mi_coche.frenar(5)
        self.assertEqual(self.mi_coche.velocidad, 15)
    def test_mostrar_informacion(self):
        info_esperada = "Marca: Ferrari, Modelo: Testarrosa, Matrícula: 1234BCD, Velocidad actual: 0 km/h"
        self.assertEqual(self.mi_coche.mostrar_informacion(), info_esperada)

    def test_calcular_t(self):
        self.mi_coche.acelerar(200)
        t_esperado = "Tiempo estimado de llegada: 0.50 horas"
        self.assertEqual(self.mi_coche.calcular_t(100), t_esperado)
        self.mi_coche.frenar(200) 
        self.assertEqual(self.mi_coche.calcular_t(100), "Infinito")
if __name__ == '__main__':
    unittest.main()
